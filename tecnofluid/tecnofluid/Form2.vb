﻿
Imports MySql.Data.MySqlClient
Public Class Form2

    Dim regis As Integer
    Dim dclick As Boolean

    Dim conexion As New MySqlConnection
    Dim comandos As New MySqlCommand
    Dim adaptador As New MySqlDataAdapter
    Dim datos As DataSet
    Dim datos5 As DataTable
    Dim datos6 As DataTable
    Private Sub Form2_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=158.69.126.163;user= tecnofluid;password=lkahd*b8asd;database=tecnofluid_tecnofluid;Convert Zero Datetime=True"
        conexion.Open()
        Me.Text = "Stock de productos"
        Label1.Text = "Costo"
        Label2.Text = "Producto"
        Label3.Text = "Precio"
        Label4.Text = "Stock"
        Label5.Text = "Porcentaje"
        Label7.Text = "Proveedor"
        Label8.Text = "Rubro"
        Label9.Text = "Código"
        Label10.Text = "U. por caja"
        Label11.Text = "Cajas"
        Label12.Text = ""
        Button1.Text = "Nuevo"
        Button2.Text = "Modificar"
        Button3.Text = "Borrar"
        'Button4.Text = "Calculos"
        Button5.Text = "Ajuste"
        GroupBox1.Text = "Actualizar costo/precio"
        GroupBox2.Text = "Registro"
        RadioButton1.Text = "ingreso"
        RadioButton2.Text = "Egreso"
        RadioButton3.Text = "Precio"
        RadioButton4.Text = "Costo"
        Label6.Text = ""

        leer()
        DataGridView1.AllowUserToAddRows = False
        colorea()
        Carga_combo()
    End Sub
    Private Sub Carga_combo()

        Dim consulta3 As String
        consulta3 = "SELECT * FROM proveedores ORDER BY razon"
        adaptador = New MySqlDataAdapter(consulta3, conexion)
        datos5 = New DataTable
        adaptador.Fill(datos5)
        With ComboBox1
            .DataSource = datos5
            .DisplayMember = "razon"
            .ValueMember = "id"
        End With
    End Sub
    Private Sub ComboBox1_TextChanged(sender As Object, e As EventArgs) Handles ComboBox1.TextChanged
        If ComboBox1.ValueMember.ToString <> "" Then
            Dim consulta4 As String
            consulta4 = "SELECT * FROM proveedores WHERE id=" + ComboBox1.SelectedValue.ToString() + ""
            adaptador = New MySqlDataAdapter(consulta4, conexion)
            datos6 = New DataTable
            adaptador.Fill(datos6)
            For Each row As DataRow In datos6.Rows
                TextBox6.Text = row("razon").ToString
                Label12.Text = row("id")
            Next
        End If
    End Sub
    Private Sub colorea() 'colorea celda de stock en estado crítico (color amarillo menor a 3 unidades, y rojo cuando no hay stock)

        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            If Me.DataGridView1.Rows(i).Cells(4).Value <= 0 Then
                Me.DataGridView1.Rows(i).Cells(4).Style.BackColor = Color.Red
            ElseIf Me.DataGridView1.Rows(i).Cells(4).Value <= 3 Then
                Me.DataGridView1.Rows(i).Cells(4).Style.BackColor = Color.Yellow
            End If
        Next
    End Sub
    Private Sub limpiar()
        Label6.Text = ""
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        Label12.Text = ""

    End Sub
    Private Sub Form2_DoubleClick(sender As Object, e As EventArgs) Handles MyBase.DoubleClick
        limpiar()
    End Sub
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click

        Dim cstock As Integer
        If TextBox10.Text <> "" Then
            cstock = TextBox10.Text * TextBox9.Text
        Else
            If TextBox4.Text = "" Then
                cstock = 0
            Else cstock = TextBox4.Text
        End If
        End If
        comandos = New MySqlCommand("INSERT INTO stock (costo,producto,precio,stock,proveedor,rubro,codigo,unidcaja,cajas)" & Chr(13) &
                                    "VALUES(@costo,@producto,@precio,@stock,@proveedor,@rubro,@codigo,@unidcaja,@cajas)", conexion)

        comandos.Parameters.AddWithValue("@costo", TextBox1.Text)
        comandos.Parameters.AddWithValue("@producto", TextBox2.Text)
        comandos.Parameters.AddWithValue("@precio", TextBox3.Text)
        comandos.Parameters.AddWithValue("@stock", cstock)
        comandos.Parameters.AddWithValue("@proveedor", TextBox6.Text)
        comandos.Parameters.AddWithValue("@rubro", TextBox7.Text)
        comandos.Parameters.AddWithValue("@codigo", TextBox8.Text)
        comandos.Parameters.AddWithValue("@unidcaja", TextBox9.Text)
        comandos.Parameters.AddWithValue("@cajas", TextBox10.Text)


        comandos.ExecuteNonQuery()


        MsgBox("¿Guardar nueva producto?")
        limpiar()
        leer()
        colorea()
    End Sub

    Private Sub leer()
        dclick = False
        Dim consulta As String
        consulta = "SELECT* from stock"
        adaptador = New MySqlDataAdapter(consulta, conexion)
        datos = New DataSet
        adaptador.Fill(datos, "stock")
        DataGridView1.DataSource = datos
        DataGridView1.DataMember = "stock"

        DataGridView1.Columns(0).Width = 0
        DataGridView1.Columns(1).Width = 50
        DataGridView1.Columns(2).Width = 175
        DataGridView1.Columns(3).Width = 50
        DataGridView1.Columns(4).Width = 50
        DataGridView1.Columns(5).Width = 175
        DataGridView1.Columns(6).Width = 150
        DataGridView1.Columns(7).Width = 100
        DataGridView1.Columns(8).Width = 85
        DataGridView1.Columns(9).Width = 75

        DataGridView1.Sort(DataGridView1.Columns(2), System.ComponentModel.ListSortDirection.Ascending)

    End Sub


    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If (e.RowIndex = -1) Then
            Return
        End If

        Dim renglon As Integer : renglon = e.RowIndex
        DataGridView1.Rows(renglon).Selected = True
        If Not IsDBNull(DataGridView1.Item(2, renglon).Value) Then
            TextBox1.Text = DataGridView1.Item(1, renglon).Value
            TextBox2.Text = DataGridView1.Item(2, renglon).Value
            TextBox3.Text = DataGridView1.Item(3, renglon).Value
            TextBox4.Text = DataGridView1.Item(4, renglon).Value
            TextBox6.Text = DataGridView1.Item(5, renglon).Value
            TextBox7.Text = DataGridView1.Item(6, renglon).Value
            TextBox8.Text = DataGridView1.Item(7, renglon).Value
            TextBox9.Text = DataGridView1.Item(8, renglon).Value
            TextBox10.Text = DataGridView1.Item(9, renglon).Value

            Label6.Text = DataGridView1.Item(0, renglon).Value
            dclick = True
            regis = renglon  'me dice en que reglon guarde'

        End If
        colorea()
    End Sub



    Private Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        If dclick = False Then
            MsgBox("Haga click en un producto")
        Else
            Dim s As Integer
            s = MsgBox("¿Borrar producto?", MsgBoxStyle.YesNo)
            If s = 6 Then  'aprete el si 
                DataGridView1.Rows.RemoveAt(regis) 'borra el reglon que elegi para pasar los datos solo borra en el data grid pero no en el archivo
                Dim eliminar As String
                eliminar = "DELETE FROM stock WHERE id='" & Label6.Text & "'"
                comandos = New MySqlCommand(eliminar, conexion)
                comandos.ExecuteNonQuery()
                limpiar()
                leer()
            End If
        End If
        colorea()
    End Sub

    Private Sub DataGridView1_CellMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseDoubleClick
        limpiar()
    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click

        If dclick = False Then
            MsgBox("Haga click en un producto")

        Else
            Dim tipo As Integer
            Dim castock As Integer
            tipo = DataGridView1.Item(4, regis).Value
            castock = DataGridView1.Item(9, regis).Value

            If TextBox10.Text <> 0 Then
                If RadioButton1.Checked = True Then
                    castock = castock + TextBox10.Text
                End If
                If RadioButton2.Checked = True Then
                    castock = castock - TextBox10.Text
                End If
                tipo = castock * TextBox9.Text
            Else
                If RadioButton1.Checked = True Then
                    tipo = tipo + TextBox4.Text
                End If
                If RadioButton2.Checked = True Then
                    tipo = tipo - TextBox4.Text
                End If
            End If
            Dim actualizar As New MySqlCommand("UPDATE  stock SET costo=@costo, producto=@producto, precio=@precio, stock=@stock, proveedor=@proveedor, rubro=@rubro, codigo=@codigo, unidcaja=@unidcaja, cajas=@cajas WHERE id='" & Label6.Text & "'", conexion)
            actualizar.Parameters.AddWithValue("@costo", TextBox1.Text)
                actualizar.Parameters.AddWithValue("@producto", TextBox2.Text)
                actualizar.Parameters.AddWithValue("@precio", TextBox3.Text)
            actualizar.Parameters.AddWithValue("@stock", tipo)
            actualizar.Parameters.AddWithValue("@proveedor", TextBox6.Text)
            actualizar.Parameters.AddWithValue("@rubro", TextBox7.Text)
            actualizar.Parameters.AddWithValue("@codigo", TextBox8.Text)
            actualizar.Parameters.AddWithValue("@unidcaja", TextBox9.Text)
            actualizar.Parameters.AddWithValue("@cajas", castock)

            actualizar.ExecuteNonQuery()

                MsgBox("Producto modificado")
            limpiar()
            leer()
            colorea()
        End If

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        If RadioButton3.Checked = True Then

            Dim actuali As New MySqlCommand("UPDATE  stock SET precio= precio*1." & TextBox5.Text, conexion)
            actuali.ExecuteNonQuery()

        End If
        If RadioButton4.Checked = True Then

            Dim actuali As New MySqlCommand("UPDATE  stock SET costo= costo*1." & TextBox5.Text, conexion)
            actuali.ExecuteNonQuery()


        End If
        leer()
        colorea()
    End Sub
End Class