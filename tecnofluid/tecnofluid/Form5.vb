﻿
Imports MySql.Data.MySqlClient

Public Class Form5
    Dim regis As Integer
    Dim dclick As Boolean

    Dim conexion As New MySqlConnection
    Dim comandos As New MySqlCommand
    Dim adaptador As New MySqlDataAdapter
    Dim datos As DataSet
    Private Sub Form5_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=158.69.126.163;user= tecnofluid;password=lkahd*b8asd;database=tecnofluid_tecnofluid;Convert Zero Datetime=True"
        conexion.Open()

        Me.Text = "Ingreso de Proveedores"
        Label1.Text = "Razón social o nombre"
        Label2.Text = "Localidad"
        Label3.Text = "Cuit/Cuil/Dni"
        Label4.Text = "Teléfono"
        Label5.Text = "Dirección"
        Label6.Text = "E-mail"
        Label7.Text = "Rubro"
        Label8.Text = "Contacto"
        Label10.Text = ""
        Button1.Text = "Nuevo"
        Button2.Text = "Modificar"
        Button3.Text = "Borrar"
        Button4.Text = "Buscar Cuit/Dni"
        GroupBox1.Text = "Búsqueda Cliente"
        leer()
        DataGridView1.AllowUserToAddRows = False
    End Sub

    Private Sub DataGridView1_CellMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseDoubleClick
        limpiar()
    End Sub
    Private Sub leer()
        dclick = False
        Dim consulta As String
        consulta = "SELECT* from proveedores"
        adaptador = New MySqlDataAdapter(consulta, conexion)
        datos = New DataSet
        adaptador.Fill(datos, "proveedores")
        DataGridView1.DataSource = datos
        DataGridView1.DataMember = "proveedores"

        DataGridView1.Columns(0).Width = 0
        DataGridView1.Columns(1).Width = 150
        DataGridView1.Columns(2).Width = 100
        DataGridView1.Columns(3).Width = 100
        DataGridView1.Columns(4).Width = 100
        DataGridView1.Columns(5).Width = 150
        DataGridView1.Columns(6).Width = 150
        DataGridView1.Columns(7).Width = 100
        DataGridView1.Columns(8).Width = 85
        DataGridView1.Sort(DataGridView1.Columns(1), System.ComponentModel.ListSortDirection.Ascending)
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If (e.RowIndex = -1) Then
            Return
        End If

        Dim renglon As Integer : renglon = e.RowIndex
        DataGridView1.Rows(renglon).Selected = True
        If Not IsDBNull(DataGridView1.Item(2, renglon).Value) Then
            TextBox1.Text = DataGridView1.Item(1, renglon).Value
            TextBox2.Text = DataGridView1.Item(2, renglon).Value
            TextBox3.Text = DataGridView1.Item(3, renglon).Value
            TextBox4.Text = DataGridView1.Item(4, renglon).Value
            TextBox5.Text = DataGridView1.Item(5, renglon).Value
            TextBox6.Text = DataGridView1.Item(6, renglon).Value
            TextBox7.Text = DataGridView1.Item(7, renglon).Value
            TextBox8.Text = DataGridView1.Item(8, renglon).Value
            Label10.Text = DataGridView1.Item(0, renglon).Value
            dclick = True
            regis = renglon  'me dice en que reglon guarde'
        End If
    End Sub
    Private Sub limpiar()
        Label10.Text = ""
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
    End Sub


    Private Sub Form5_DoubleClick(sender As Object, e As EventArgs) Handles MyBase.DoubleClick
        limpiar()
    End Sub
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click

        comandos = New MySqlCommand("INSERT INTO proveedores (razon,localidad,cuit,telefono,direccion,email,rubro,contacto)" & Chr(13) &
                                    "VALUES(@razon,@localidad,@cuit,@telefono,@direccion,@email,@rubro,@contacto )", conexion)

        comandos.Parameters.AddWithValue("@razon", TextBox1.Text)
        comandos.Parameters.AddWithValue("@localidad", TextBox2.Text)
        comandos.Parameters.AddWithValue("@cuit", TextBox3.Text)
        comandos.Parameters.AddWithValue("@telefono", TextBox4.Text)
        comandos.Parameters.AddWithValue("@direccion", TextBox5.Text)
        comandos.Parameters.AddWithValue("@email", TextBox6.Text)
        comandos.Parameters.AddWithValue("@rubro", TextBox7.Text)
        comandos.Parameters.AddWithValue("@contacto", TextBox8.Text)
        comandos.ExecuteNonQuery()

        MsgBox("¿Desea guardar nuevo proveedor?")
        limpiar()
        leer()
    End Sub

    Private Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        If dclick = False Then
            MsgBox("Haga click en un proveedor")
        Else
            Dim s As Integer
            s = MsgBox("¿Borrar proveedor?", MsgBoxStyle.YesNo)
            If s = 6 Then  'aprete el si 
                DataGridView1.Rows.RemoveAt(regis) 'borra el reglon que elegi para pasar los datos solo borra en el data grid pero no en el archivo
                Dim eliminar As String
                eliminar = "DELETE FROM proveedores WHERE id='" & Label10.Text & "'"
                comandos = New MySqlCommand(eliminar, conexion)
                comandos.ExecuteNonQuery()
            End If
        End If
        limpiar()
        leer()
    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click
        If dclick = False Then
            MsgBox("Haga click en un proveedor")
        Else

            Dim actualizar As New MySqlCommand("UPDATE  proveedores SET razon=@razon, localidad=@localidad, cuit=@cuit, telefono=@telefono, direccion=@direccion, email=@email, rubro=@rubro, contacto=@contacto WHERE id='" & Label10.Text & "'", conexion)

            actualizar.Parameters.AddWithValue("@razon", TextBox1.Text)
            actualizar.Parameters.AddWithValue("@localidad", TextBox2.Text)
            actualizar.Parameters.AddWithValue("@cuit", TextBox3.Text)
            actualizar.Parameters.AddWithValue("@telefono", TextBox4.Text)
            actualizar.Parameters.AddWithValue("@direccion", TextBox5.Text)
            actualizar.Parameters.AddWithValue("@email", TextBox6.Text)
            actualizar.Parameters.AddWithValue("@rubro", TextBox7.Text)
            actualizar.Parameters.AddWithValue("@contacto", TextBox8.Text)
            actualizar.ExecuteNonQuery()

            MsgBox("Proveedor modificado")
            limpiar()
            leer()

        End If

    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Rows(i).Cells(3).Value = TextBox9.Text Then

                DataGridView1.Rows(i).Selected = True
            End If
        Next

        Dim consulta As String
        Dim lista As Byte
        If TextBox9.Text <> "" Then
            consulta = "SELECT * FROM proveedores WHERE cuit ='" & TextBox9.Text & "'"
            adaptador = New MySqlDataAdapter(consulta, conexion)
            datos = New DataSet
            adaptador.Fill(datos, "proveedores")
            lista = datos.Tables("proveedores").Rows.Count
        End If
        If lista <> 0 Then
            TextBox1.Text = datos.Tables("proveedores").Rows(0).Item("razon")
            TextBox2.Text = datos.Tables("proveedores").Rows(0).Item("localidad")
            TextBox3.Text = datos.Tables("proveedores").Rows(0).Item("cuit")
            TextBox4.Text = datos.Tables("proveedores").Rows(0).Item("telefono")
            TextBox5.Text = datos.Tables("proveedores").Rows(0).Item("direccion")
            TextBox6.Text = datos.Tables("proveedores").Rows(0).Item("email")
            TextBox7.Text = datos.Tables("proveedores").Rows(0).Item("rubro")
            TextBox8.Text = datos.Tables("proveedores").Rows(0).Item("contacto")

        Else
            MsgBox("datos no encontrados")
        End If

    End Sub

End Class