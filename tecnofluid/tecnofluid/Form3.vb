﻿Imports MySql.Data.MySqlClient

Public Class Form3
    Dim dato(2, 11) As String
    Dim renglon1 As Integer
    Dim renglon2 As String

    Dim regis As Integer
    Dim dclick As Boolean

    Dim conexion As New MySqlConnection
    Dim comandos As New MySqlCommand
    Dim adaptador As New MySqlDataAdapter
    Dim datos As DataSet
    Dim datos1 As DataTable
    Dim datos2 As DataTable
    Dim datos3 As DataTable
    Dim datos4 As DataTable
    Dim datos5 As DataTable
    Dim datos6 As DataTable

    Private Sub Form3_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=158.69.126.163;user= tecnofluid;password=lkahd*b8asd;database=tecnofluid_tecnofluid;Convert Zero Datetime=True"
        conexion.Open()
        Me.Text = "Ingresos y Egresos"

        Label1.Text = "Hasta"
        Label2.Text = "Concepto"
        Label3.Text = "Tipo"
        Label4.Text = "Importe"
        Label5.Text = "Desde"
        Label6.Text = ""
        Label7.Text = "Cantidad"
        Label9.Text = "Productos"
        Label8.Text = ""
        Label10.Text = ""
        Label11.Text = "Stock Actual"
        Label12.Text = "Cajas"
        Label13.Text = "Cliente"
        Label14.Text = "Proveedor"
        Label15.Text = "Fecha de Entrega"
        Label16.Text = "Vencimiento"
        Label17.Text = "Fecha de Pago"
        Label18.Text = "Remito"
        Label19.Text = "Factura"
        Label20.Text = "Condición"
        Label21.Text = "Ret. Ingreso Bruto"
        Label22.Text = "Medio de Pago"
        Label23.Text = "Comprobante"
        Label24.Text = "Venta Neta"
        Label25.Text = "Total"
        Label26.Text = ""
        Label27.Text = ""
        Label28.Text = ""
        Label29.Text = ""

        Button1.Text = "Nuevo"
        Button2.Text = "Modificar"
        Button3.Text = "Borrar"
        Button4.Text = "Total"
        Button5.Text = "Volver"
        Button6.Text = "Adjuntar"
        Button7.Text = "Stock"
        Button8.Text = "Clientes"
        Button9.Text = "Proveedores"


        RadioButton1.Text = "Ingreso"
        RadioButton2.Text = "Egreso"
        GroupBox1.Text = "Informe"
        GroupBox2.Text = "Registro"

        ComboBox4.Items.Add("+0")
        ComboBox4.Items.Add("+15")
        ComboBox4.Items.Add("+21")
        ComboBox4.Items.Add("+30")

        ComboBox5.Items.Add("C")
        ComboBox5.Items.Add("T")
        ComboBox5.Items.Add("E")

        Carga_combo2()
        Carga_combo1()
        Carga_combo3()
        leer()
        DataGridView1.AllowUserToAddRows = False
    End Sub
    Private Sub Carga_combo2()
        Dim consulta1 As String
        consulta1 = "SELECT * FROM stock ORDER BY producto"
        adaptador = New MySqlDataAdapter(consulta1, conexion)
        datos1 = New DataTable
        adaptador.Fill(datos1)
        With ComboBox2
            .DataSource = datos1
            .DisplayMember = "producto"
            .ValueMember = "id"
        End With
    End Sub
    Private Sub Carga_combo1()
        Dim consulta3 As String
        consulta3 = "SELECT * FROM clientes ORDER BY razon"
        adaptador = New MySqlDataAdapter(consulta3, conexion)
        datos3 = New DataTable
        adaptador.Fill(datos3)
        With ComboBox1
            .DataSource = datos3
            .DisplayMember = "razon"
            .ValueMember = "id"
        End With
    End Sub
    Private Sub Carga_combo3()
        Dim consulta3 As String
        consulta3 = "SELECT * FROM proveedores ORDER BY razon"
        adaptador = New MySqlDataAdapter(consulta3, conexion)
        datos5 = New DataTable
        adaptador.Fill(datos5)
        With ComboBox3
            .DataSource = datos5
            .DisplayMember = "razon"
            .ValueMember = "id"
        End With
    End Sub
    Private Sub Select_Items(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.DropDownClosed
        TextBox9.Text = Convert.ToString(ComboBox4.SelectedItem)
    End Sub
    Private Sub Select_Items1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox5.DropDownClosed
        TextBox10.Text = Convert.ToString(ComboBox5.SelectedItem)
    End Sub
    Private Sub ComboBox1_TextChanged(sender As Object, e As EventArgs) Handles ComboBox1.TextChanged
        If ComboBox1.ValueMember.ToString <> "" Then
            Dim consulta4 As String
            consulta4 = "SELECT * FROM clientes WHERE id=" + ComboBox1.SelectedValue.ToString() + ""
            adaptador = New MySqlDataAdapter(consulta4, conexion)
            datos4 = New DataTable
            adaptador.Fill(datos4)
            For Each row As DataRow In datos4.Rows
                TextBox5.Text = row("razon").ToString
                Label26.Text = row("id")
            Next
        End If
    End Sub
    Private Sub ComboBox3_TextChanged(sender As Object, e As EventArgs) Handles ComboBox3.TextChanged
        If ComboBox3.ValueMember.ToString <> "" Then
            Dim consulta4 As String
            consulta4 = "SELECT * FROM proveedores WHERE id=" + ComboBox3.SelectedValue.ToString() + ""
            adaptador = New MySqlDataAdapter(consulta4, conexion)
            datos6 = New DataTable
            adaptador.Fill(datos6)
            For Each row As DataRow In datos6.Rows
                TextBox6.Text = row("razon").ToString
                Label27.Text = row("id")
            Next
        End If
    End Sub

    Private Sub ComboBox2_TextChanged(sender As Object, e As EventArgs) Handles ComboBox2.TextChanged
        If ComboBox2.ValueMember.ToString <> "" Then
            Dim consulta2 As String
            consulta2 = "SELECT * FROM stock WHERE id=" + ComboBox2.SelectedValue.ToString() + ""
            adaptador = New MySqlDataAdapter(consulta2, conexion)
            datos2 = New DataTable
            adaptador.Fill(datos2)
            For Each row As DataRow In datos2.Rows
                TextBox2.Text = row("producto").ToString
                Label10.Text = row("stock")
                Label8.Text = row("id")
                Label28.Text = row("cajas")
                Label29.Text = row("unidcaja")
            Next
        End If
    End Sub
    Shared Function Imagen_Bytes(ByVal Foto As Image) As Byte()
        If Not Foto Is Nothing Then
            Dim Codi As New IO.MemoryStream
            Foto.Save(Codi, Imaging.ImageFormat.Jpeg)
            Return Codi.GetBuffer
        Else
            Return Nothing
        End If
    End Function
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim tipo As String : tipo = "E"
        If RadioButton1.Checked = True Then
            tipo = "I"
        End If
        Dim valorvta As String
        valorvta = TextBox14.Text
        If TextBox13.Text <> "" Then
            valorvta = TextBox14.Text - TextBox13.Text
        End If

        Dim importes As String
        importes = TextBox3.Text
        If TextBox3.Text = "" Then
            importes = valorvta
        End If

        Dim cantt As String
        cantt = TextBox4.Text
        If TextBox4.Text = "" Then
            If Label29.Text = "" Or 0 Then
                cantt = TextBox4.Text
            Else cantt = Label29.Text * TextBox1.Text
            End If
        End If

        Dim ximagen As Byte()
        ximagen = Imagen_Bytes(Me.PictureBox1.Image)

        comandos = New MySqlCommand("INSERT INTO ingresos (fecha,concepto,tipo,importe,cantidad,cliente,proveedor,fechaEntrega,fechaVto,fechaPago,remito,factura,condicion,retencionIG,medioPago,comprobante,valorVta,total)" & Chr(13) &
                                    "VALUES(@fecha,@concepto,@tipo,@importe,@cantidad,@cliente,@proveedor,@fechaEntrega,@fechaVto,@fechaPago,@remito,@factura,@condicion,@retencionIG,@medioPago,@comprobante,@valorVta,@total )", conexion)

        comandos.Parameters.AddWithValue("@fecha", DateTimePicker1.Value)
        comandos.Parameters.AddWithValue("@fechaEntrega", DateTimePicker4.Value)
        comandos.Parameters.AddWithValue("@fechaVto", DateTimePicker5.Value)
        comandos.Parameters.AddWithValue("@fechaPago", DateTimePicker6.Value)
        comandos.Parameters.AddWithValue("@concepto", TextBox2.Text)
        comandos.Parameters.AddWithValue("@tipo", tipo)
        comandos.Parameters.AddWithValue("@importe", importes)
        comandos.Parameters.AddWithValue("@cantidad", cantt)
        comandos.Parameters.AddWithValue("@cliente", TextBox5.Text)
        comandos.Parameters.AddWithValue("@proveedor", TextBox6.Text)
        comandos.Parameters.AddWithValue("@remito", TextBox7.Text)
        comandos.Parameters.AddWithValue("@factura", TextBox8.Text)
        comandos.Parameters.AddWithValue("@condicion", TextBox9.Text)
        comandos.Parameters.AddWithValue("@retencionIG", TextBox13.Text)
        comandos.Parameters.AddWithValue("@medioPago", TextBox10.Text)
        comandos.Parameters.AddWithValue("@comprobante", ximagen)
        comandos.Parameters.AddWithValue("@valorVta", valorvta)
        comandos.Parameters.AddWithValue("@total", TextBox14.Text)

        comandos.ExecuteNonQuery()

        Dim res As String
        res = Label8.Text
        If res <> "" Then
            Dim unidcaja As Integer
            unidcaja = Label29.Text
            If unidcaja <> 0 Then
                Dim cantcaja As Integer
                Dim caja As Integer
                Dim tipona As Integer
                Dim cc As Integer
                cc = TextBox1.Text
                cantcaja = unidcaja * cc
                tipona = Label10.Text
                caja = Label28.Text

                If RadioButton1.Checked = True Then
                    tipona = tipona - cantcaja
                    caja = caja - TextBox1.Text
                End If
                If RadioButton2.Checked = True Then
                    tipona = tipona + cantcaja
                    caja = caja + TextBox1.Text
                End If

                Dim actualizar3 As New MySqlCommand("UPDATE  stock SET  stock=@stock, cajas=@cajas WHERE id='" & Label8.Text & "'", conexion)

                actualizar3.Parameters.AddWithValue("@stock", tipona)
                actualizar3.Parameters.AddWithValue("@cajas", caja)

                actualizar3.ExecuteNonQuery()
            Else
                Dim tipona As Integer
                tipona = Label10.Text

                If RadioButton1.Checked = True Then
                    tipona = tipona - TextBox4.Text
                End If
                If RadioButton2.Checked = True Then
                    tipona = tipona + TextBox4.Text
                End If

                Dim actualizar3 As New MySqlCommand("UPDATE  stock SET  stock=@stock WHERE id='" & Label8.Text & "'", conexion)

                actualizar3.Parameters.AddWithValue("@stock", tipona)

                actualizar3.ExecuteNonQuery()
            End If
        End If
        MsgBox("¿Guardar nuevo ingreso?")
        limpiar()
        leer()
    End Sub

    Private Sub leer()
        dclick = False
        Dim consulta As String
        consulta = "SELECT* from ingresos"
        adaptador = New MySqlDataAdapter(consulta, conexion)
        datos = New DataSet
        adaptador.Fill(datos, "ingresos")
        DataGridView1.DataSource = datos
        DataGridView1.DataMember = "ingresos"
        Me.DataGridView1.Columns(1).DefaultCellStyle.Format = "dd/MM/yyyy HH:mm"
        DataGridView1.Columns(0).Width = 0
        DataGridView1.Columns(1).Width = 98
        DataGridView1.Columns(2).Width = 100
        DataGridView1.Columns(3).Width = 30
        DataGridView1.Columns(4).Width = 50
        DataGridView1.Columns(5).Width = 50
        DataGridView1.Columns(6).Width = 100
        DataGridView1.Columns(7).Width = 100
        DataGridView1.Columns(8).Width = 75
        DataGridView1.Columns(9).Width = 75
        DataGridView1.Columns(10).Width = 75
        DataGridView1.Columns(11).Width = 0
        DataGridView1.Columns(12).Width = 0
        DataGridView1.Columns(13).Width = 0
        DataGridView1.Columns(14).Width = 57
        DataGridView1.Columns(15).Width = 0
        DataGridView1.Columns(16).Width = 0
        DataGridView1.Columns(17).Width = 50
        DataGridView1.Columns(18).Width = 55

        DataGridView1.Sort(DataGridView1.Columns(1), System.ComponentModel.ListSortDirection.Descending)

        colorea()
    End Sub
    Private Sub limpiar()
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox12.Text = ""
        TextBox13.Text = ""
        TextBox14.Text = ""
        DateTimePicker1.Value = Now
        DateTimePicker7.Refresh()
        DateTimePicker2.Value = DateTimePicker7.Value
        DateTimePicker3.Value = DateTimePicker7.Value
        DateTimePicker4.Value = DateTimePicker7.Value
        DateTimePicker5.Value = DateTimePicker7.Value
        DateTimePicker6.Value = DateTimePicker7.Value
        Carga_combo2()
        Carga_combo1()
        Carga_combo3()
        PictureBox1.Image = Nothing
        Label10.Text = ""
        Label26.Text = ""
        Label27.Text = ""
        Label28.Text = ""
        Label29.Text = ""
        Label6.Text = ""
        Label8.Text = ""
    End Sub
    Private Sub Form3_DoubleClick(sender As Object, e As EventArgs) Handles MyBase.DoubleClick
        limpiar()
    End Sub

    Private Sub calculo() 'suma totales de ingreso y egreso dependiendo de la condición
        Dim total1 As Integer
        Dim total2 As Integer
        Dim cheque As Integer
        Dim cheque1 As Integer
        Dim efectivo As Integer
        Dim efectivo1 As Integer
        Dim transferencia As Integer
        Dim transferencia1 As Integer

        Dim desde As String
        Dim hasta As String

        desde = DateTimePicker2.Value.ToString("yyyy-MM-dd HH:mm:ss")
        hasta = DateTimePicker3.Value.ToString("yyyy-MM-dd HH:mm:ss")
        Dim sql As String = "select * from ingresos where fecha between'" + desde + "' and '" + hasta + "' "

        Dim adaptador2 As New MySqlDataAdapter(sql, conexion)
        Dim tblFiltro As New DataTable()

        adaptador2.Fill(tblFiltro)
        DataGridView1.DataSource = tblFiltro
        DataGridView1.Sort(DataGridView1.Columns(3), System.ComponentModel.ListSortDirection.Descending)


        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1

            If Me.DataGridView1.Rows(i).Cells(3).Value = "I" Then
                total1 += Val(Me.DataGridView1.Rows(i).Cells(4).Value)
                If Me.DataGridView1.Rows(i).Cells(15).Value = "E" Then
                    efectivo += Val(Me.DataGridView1.Rows(i).Cells(12).Value)
                ElseIf Me.DataGridView1.Rows(i).Cells(15).Value = "C" Then
                    cheque += Val(Me.DataGridView1.Rows(i).Cells(12).Value)
                Else transferencia += Val(Me.DataGridView1.Rows(i).Cells(12).Value)
                End If
            Else
                total2 += Val(Me.DataGridView1.Rows(i).Cells(4).Value)
                If Me.DataGridView1.Rows(i).Cells(15).Value = "E" Then
                    efectivo1 += Val(Me.DataGridView1.Rows(i).Cells(12).Value)
                ElseIf Me.DataGridView1.Rows(i).Cells(15).Value = "C" Then
                    cheque1 += Val(Me.DataGridView1.Rows(i).Cells(12).Value)
                Else transferencia1 += Val(Me.DataGridView1.Rows(i).Cells(12).Value)
                End If
            End If
        Next
        MsgBox("Ingresos total: " & total1 & Chr(13) + Chr(10) & "Ingresos-efectivo: " & efectivo & Chr(13) + Chr(10) & "Ingresos-cheque: " & cheque & Chr(13) + Chr(10) & "Ingresos-transferencia: " & transferencia & Chr(13) + Chr(10) & "Egresos total: " & total2 & Chr(13) + Chr(10) & "Egresos-efectivo: " & efectivo1 & Chr(13) + Chr(10) & "Egresos-cheque: " & cheque1 & Chr(13) + Chr(10) & "Egresos-transferencia: " & transferencia1)
    End Sub

    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If (e.RowIndex = -1) Then
            Return
        End If

        Dim renglon As Integer : renglon = e.RowIndex
        DataGridView1.Rows(renglon).Selected = True
        If Not IsDBNull(DataGridView1.Item(2, renglon).Value) Then
            TextBox2.Text = DataGridView1.Item(2, renglon).Value
            TextBox3.Text = DataGridView1.Item(4, renglon).Value
            TextBox4.Text = DataGridView1.Item(5, renglon).Value
            TextBox5.Text = DataGridView1.Item(6, renglon).Value
            TextBox6.Text = DataGridView1.Item(7, renglon).Value
            TextBox7.Text = DataGridView1.Item(11, renglon).Value
            TextBox8.Text = DataGridView1.Item(12, renglon).Value
            TextBox9.Text = DataGridView1.Item(13, renglon).Value
            TextBox13.Text = DataGridView1.Item(14, renglon).Value
            TextBox14.Text = DataGridView1.Item(17, renglon).Value
            TextBox12.Text = DataGridView1.Item(18, renglon).Value
            TextBox10.Text = DataGridView1.Item(15, renglon).Value
            Dim zimagen As Image
            If DataGridView1.Item(16, renglon).Value Is DBNull.Value Then
                PictureBox1.Image = Nothing
            Else
                zimagen = Bytes_Imagen(DataGridView1.Item(16, renglon).Value)
                PictureBox1.Image = zimagen
            End If

            'TextBox11.Text = DataGridView1.Item(16, renglon).Value
            DateTimePicker1.Value = DataGridView1.Item(1, renglon).Value
            DateTimePicker4.Value = DataGridView1.Item(8, renglon).Value
            DateTimePicker5.Value = DataGridView1.Item(9, renglon).Value
            DateTimePicker6.Value = DataGridView1.Item(10, renglon).Value

            Label6.Text = DataGridView1.Item(0, renglon).Value
            Dim tipo As String = DataGridView1.Item(3, renglon).Value
            If tipo = "E" Then
                RadioButton2.Checked = True
            Else
                RadioButton1.Checked = True
            End If
            dclick = True
            regis = renglon  'me dice en que reglon guarde'
        End If
        colorea()
    End Sub

    Shared Function Bytes_Imagen(ByVal Foto As Byte()) As Image
        If Not Foto Is Nothing Then
            Dim Codi As New IO.MemoryStream(Foto)
            Dim resultado As Image = Image.FromStream(Codi)
            Return resultado
        Else
            Return Nothing
        End If
    End Function
    Private Sub Button3_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button3.Click
        If dclick = False Then
            MsgBox("haga click en un registro")
        Else
            Dim s As Integer
            s = MsgBox("borra este registro", MsgBoxStyle.YesNo)
            If s = 6 Then  'aprete el si 
                DataGridView1.Rows.RemoveAt(regis) 'borra el reglon que elegi para pasar los datos solo borra en el data grid pero no en el archivo
                Dim eliminar As String
                eliminar = "DELETE FROM ingresos WHERE id='" & Label6.Text & "'"
                comandos = New MySqlCommand(eliminar, conexion)
                comandos.ExecuteNonQuery()
                limpiar()
                leer()
            End If
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button2.Click

        If dclick = False Then
            MsgBox("haga click en un registro")
        Else

            Dim tipo As String = "E"
            If RadioButton1.Checked = True Then
                tipo = "I"
            End If
            Dim ximagen As Byte()
            ximagen = Imagen_Bytes(Me.PictureBox1.Image)

            Dim valorvta As String
            valorvta = TextBox14.Text
            If TextBox13.Text <> 0 Then
                valorvta = TextBox14.Text - TextBox13.Text
            End If

            Dim importes As String
            importes = TextBox3.Text
            If valorvta <> 0 Then
                importes = valorvta
            End If

            Dim cantt As String
            cantt = TextBox4.Text
            If Label29.Text = "" Or 0 Then
                    cantt = TextBox4.Text
                Else cantt = Label29.Text * TextBox1.Text
                End If

            Dim actualizar As New MySqlCommand("UPDATE  ingresos SET concepto=@concepto, tipo=@tipo, importe=@importe, fecha=@fecha, fechaEntrega=@fechaEntrega, fechaVto=@fechaVto, fechaPago=@fechaPago, cantidad=@cantidad, cliente=@cliente, proveedor=@proveedor, remito=@remito, factura=@factura, condicion=@condicion, retencionIG=@retencionIG, medioPago=@medioPago, comprobante=@comprobante, valorVta=@valorVta, total=@total WHERE id='" & Label6.Text & "'", conexion)
            actualizar.Parameters.AddWithValue("@concepto", TextBox2.Text)
            actualizar.Parameters.AddWithValue("@importe", importes)
            actualizar.Parameters.AddWithValue("@tipo", tipo)

            actualizar.Parameters.AddWithValue("@fecha", DateTimePicker1.Value)
            actualizar.Parameters.AddWithValue("@fechaEntrega", DateTimePicker4.Value)
            actualizar.Parameters.AddWithValue("@fechaVto", DateTimePicker5.Value)
            actualizar.Parameters.AddWithValue("@fechaPago", DateTimePicker6.Value)

            actualizar.Parameters.AddWithValue("@cantidad", cantt)
            actualizar.Parameters.AddWithValue("@cliente", TextBox5.Text)
            actualizar.Parameters.AddWithValue("@proveedor", TextBox6.Text)
            actualizar.Parameters.AddWithValue("@remito", TextBox7.Text)
            actualizar.Parameters.AddWithValue("@factura", TextBox8.Text)
            actualizar.Parameters.AddWithValue("@condicion", TextBox9.Text)
            actualizar.Parameters.AddWithValue("@retencionIG", TextBox13.Text)
            actualizar.Parameters.AddWithValue("@medioPago", TextBox10.Text)
            actualizar.Parameters.AddWithValue("@comprobante", ximagen)
            actualizar.Parameters.AddWithValue("@valorVta", valorvta)
            actualizar.Parameters.AddWithValue("@total", TextBox14.Text)

            actualizar.ExecuteNonQuery()
            Dim res As String
            res = Label8.Text
            If res <> "" Then
                Dim unidcaja As Integer
                unidcaja = Label29.Text
                If unidcaja <> 0 Then
                    Dim cantcaja As Integer
                    Dim caja As Integer
                    Dim tipona As Integer
                    Dim cc As String
                    cc = TextBox1.Text
                    cantcaja = unidcaja * cc
                    tipona = Label10.Text
                    caja = Label28.Text

                    If RadioButton1.Checked = True Then
                        tipona = tipona - cantcaja
                        caja = caja - TextBox1.Text
                    End If
                    If RadioButton2.Checked = True Then
                        tipona = tipona + cantcaja
                        caja = caja + TextBox1.Text
                    End If

                    Dim actualizar3 As New MySqlCommand("UPDATE  stock SET  stock=@stock, cajas=@cajas WHERE id='" & Label8.Text & "'", conexion)

                    actualizar3.Parameters.AddWithValue("@stock", tipona)
                    actualizar3.Parameters.AddWithValue("@cajas", caja)

                    actualizar3.ExecuteNonQuery()

                Else
                    Dim tipona As Integer

                    tipona = Label10.Text

                    If RadioButton1.Checked = True Then
                        tipona = tipona - TextBox4.Text
                    End If
                    If RadioButton2.Checked = True Then
                        tipona = tipona + TextBox4.Text
                    End If

                    Dim actualizar3 As New MySqlCommand("UPDATE  stock SET  stock=@stock WHERE id='" & Label8.Text & "'", conexion)

                    actualizar3.Parameters.AddWithValue("@stock", tipona)

                    actualizar3.ExecuteNonQuery()
                End If
            End If
            MsgBox("Ingreso modificado")
            limpiar()
            leer()
        End If
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        calculo()
    End Sub
    Private Sub colorea() 'colorea celda de Tipo (Ingreso color verde, y rojo cuando no es Egreso)
        Dim vencimiento As String
        vencimiento = DateTimePicker7.Value.ToString("dd-MM-yyyy")
        For i As Integer = 0 To Me.DataGridView1.Rows.Count - 1
            If Me.DataGridView1.Rows(i).Cells(3).Value = "E" Then
                Me.DataGridView1.Rows(i).Cells(3).Style.BackColor = Color.Red
            ElseIf Me.DataGridView1.Rows(i).Cells(3).Value = "I" Then
                Me.DataGridView1.Rows(i).Cells(3).Style.BackColor = Color.Lime
            End If
            If Me.DataGridView1.Rows(i).Cells(9).Value < vencimiento And Me.DataGridView1.Rows(i).Cells(10).Value <= Me.DataGridView1.Rows(i).Cells(9).Value Then
                Me.DataGridView1.Rows(i).Cells(9).Style.BackColor = Color.Red
            ElseIf Me.DataGridView1.Rows(i).Cells(9).Value = vencimiento Then
                Me.DataGridView1.Rows(i).Cells(9).Style.BackColor = Color.Yellow
            End If
            If Me.DataGridView1.Rows(i).Cells(10).Value > Me.DataGridView1.Rows(i).Cells(9).Value Then
                Me.DataGridView1.Rows(i).Cells(9).Style.BackColor = Color.Lime
                Me.DataGridView1.Rows(i).Cells(10).Style.BackColor = Color.Lime

            End If
        Next
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        leer()
        DateTimePicker2.Value = DateTimePicker7.Value
        DateTimePicker3.Value = DateTimePicker7.Value
        limpiar()
        colorea()
    End Sub
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim file As New OpenFileDialog()
        file.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG"
        If file.ShowDialog() = DialogResult.OK Then
            PictureBox1.Image = Image.FromFile(file.FileName)
        End If
    End Sub
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Form2.Show()
    End Sub
    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Form4.Show()
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Form5.Show()
    End Sub

    Private Sub DataGridView1_CellMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseDoubleClick
        Form7.Show()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        If Me.PictureBox1.Image IsNot Nothing Then
            Me.PictureBox1.Image.Save(IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.MyPictures, "Comprobante.jpg"))
        End If
    End Sub
End Class