﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form6
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form6))
        Me.txt_ruta = New System.Windows.Forms.TextBox()
        Me.Cargar = New System.Windows.Forms.Button()
        Me.Ruta = New System.Windows.Forms.Label()
        Me.ArcPDF = New AxAcroPDFLib.AxAcroPDF()
        CType(Me.ArcPDF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_ruta
        '
        Me.txt_ruta.Location = New System.Drawing.Point(12, 55)
        Me.txt_ruta.Name = "txt_ruta"
        Me.txt_ruta.Size = New System.Drawing.Size(223, 20)
        Me.txt_ruta.TabIndex = 0
        '
        'Cargar
        '
        Me.Cargar.Location = New System.Drawing.Point(12, 93)
        Me.Cargar.Name = "Cargar"
        Me.Cargar.Size = New System.Drawing.Size(75, 23)
        Me.Cargar.TabIndex = 1
        Me.Cargar.Text = "Button1"
        Me.Cargar.UseVisualStyleBackColor = True
        '
        'Ruta
        '
        Me.Ruta.AutoSize = True
        Me.Ruta.Location = New System.Drawing.Point(12, 22)
        Me.Ruta.Name = "Ruta"
        Me.Ruta.Size = New System.Drawing.Size(39, 13)
        Me.Ruta.TabIndex = 3
        Me.Ruta.Text = "Label1"
        '
        'ArcPDF
        '
        Me.ArcPDF.Enabled = True
        Me.ArcPDF.Location = New System.Drawing.Point(37, 135)
        Me.ArcPDF.Name = "ArcPDF"
        Me.ArcPDF.OcxState = CType(resources.GetObject("ArcPDF.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ArcPDF.Size = New System.Drawing.Size(726, 284)
        Me.ArcPDF.TabIndex = 4
        '
        'Form6
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.ArcPDF)
        Me.Controls.Add(Me.Ruta)
        Me.Controls.Add(Me.Cargar)
        Me.Controls.Add(Me.txt_ruta)
        Me.Name = "Form6"
        Me.Text = "Form6"
        CType(Me.ArcPDF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_ruta As TextBox
    Friend WithEvents Cargar As Button
    Friend WithEvents Ruta As Label
    Friend WithEvents ArcPDF As AxAcroPDFLib.AxAcroPDF
End Class
